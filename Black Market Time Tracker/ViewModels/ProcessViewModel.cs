﻿using Black_Market_Time_Tracker.Models;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace Black_Market_Time_Tracker.ViewModels
{
    public class ProcessViewModel
    {
        public DelegateCommand ItemSelectedCommand { get; }

        public ObservableCollection<ProcessEntry> ProcessList { get; } 
            = new ObservableCollection<ProcessEntry>();

        public ProcessEntry SelectedProcessEntry { get; set; }

        public ProcessViewModel()
        {
            ItemSelectedCommand = new DelegateCommand(OnProcessSelected);
            RequeryProcesses();
        }

        public void RequeryProcesses()
        {
            ProcessList.Clear();

            Process[] allProcess = Process.GetProcesses().Where(p => p.ProcessName.Equals("Wow") ||
                                                                      p.ProcessName.Equals("Wow-64")).ToArray();
            foreach (var process in allProcess)
            {
                ProcessEntry entry = new ProcessEntry(process);
                ProcessList.Add(entry);
            }
        }

        public event EventHandler ProcessSelected;

        private void OnProcessSelected(object sender)
        {
            if (SelectedProcessEntry != null)
            {
                ProcessSelected?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}