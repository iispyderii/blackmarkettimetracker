﻿using Black_Market_Time_Tracker.Models;
using Black_Market_Time_Tracker.ViewModels;
using PropertyChanged;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Black_Market_Time_Tracker
{
    [ImplementPropertyChanged]
    public class MainViewModel
    {
        private Process wowProcess;
        private ItemReader itemReader;

        public DelegateCommand RefreshItemsCommand { get; set; }
        public DelegateCommand PickProcessCommand { get; set; }

        public ObservableCollection<BlackMarketItem> BlackMarketItems { get; set; } 
            = new ObservableCollection<BlackMarketItem>();

        public Dictionary<string, Locale> Locales { get; }

        public Locale SelectedLocale { get; set; }

        public bool ProcessFlyoutOpen { get; set; }

        public ProcessViewModel ProcessSelectViewModel { get; set; } 
            = new ProcessViewModel();

        public UnsupportedVersionViewModel UnsupportedViewModel { get; set; }
            = new UnsupportedVersionViewModel();

        public bool UnsupportedVersionFlyoutOpen { get; set; }

        public CollectionViewSource SortedItems { get; set; }
            = new CollectionViewSource();

        public bool WowProcessActive { get; set; } = false;

        public MainViewModel()
        {
            Locales = LocaleDatabase.GetLocales();
            RefreshItemsCommand = new DelegateCommand(RefreshItems_OnExecute);
            PickProcessCommand = new DelegateCommand(PickProcess_OnExecute);

            // Handle events
            ProcessSelectViewModel.ProcessSelected += Process_Changed;
            UnsupportedViewModel.Accepted += UnsupportedViewModel_Accepted;
            UnsupportedViewModel.Exited += UnsupportedViewModel_Exited;

            SortedItems.SortDescriptions.Add(new SortDescription("Index", ListSortDirection.Ascending));
            SortedItems.Source = BlackMarketItems;

            ProcessFlyoutOpen = true;
        }

        private void PickProcess_OnExecute(object sender)
        {
            ProcessSelectViewModel.RequeryProcesses();

            ProcessFlyoutOpen = true;
        }

        private void Process_Changed(object sender, EventArgs e)
        {
            wowProcess = ProcessSelectViewModel.SelectedProcessEntry.Process;
            itemReader = new ItemReader(ProcessSelectViewModel.SelectedProcessEntry);
            UnsupportedVersionFlyoutOpen = !itemReader.IsSupportedVersion;
            WowProcessActive = true;

            // Clean up UI
            ProcessFlyoutOpen = false;
            BlackMarketItems.Clear();
        }

        private void RefreshItems_OnExecute(object sender)
        {
            BlackMarketItems.Clear();
            var itemsToAdd = new ConcurrentBag<BlackMarketItem>();
            var items = itemReader.ReadItems();
            Parallel.For(0, items.Count, i =>
            //Parallel.For(0, 8, i =>
            //var i = 0;
            //foreach (var item in items)
            {
                BlackMarketItem displayableItem = new BlackMarketItem(i, items[i], SelectedLocale);
                //var displayableItem = new BlackMarketItem(i);
                itemsToAdd.Add(displayableItem);
                //i++;
                });
            //}
            foreach (var displayableItem in itemsToAdd)
            {
                 BlackMarketItems.Add(displayableItem);
            }

            //for (int i = 0; i < 8; i++)
            //{
            //    var displayableItem = new BlackMarketItem(i);
            //    BlackMarketItems.Add(displayableItem);
            //}
        }

        private void UnsupportedViewModel_Accepted(object sender, EventArgs e)
        {
            UnsupportedVersionFlyoutOpen = false;
        }

        private void UnsupportedViewModel_Exited(object sender, EventArgs e)
        {
            Application.Current.MainWindow.Close();
        }
    }
}
