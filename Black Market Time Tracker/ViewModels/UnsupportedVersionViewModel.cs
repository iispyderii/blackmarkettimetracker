﻿using Black_Market_Time_Tracker.Models;
using System;

namespace Black_Market_Time_Tracker.ViewModels
{
    public class UnsupportedVersionViewModel
    {
        public event EventHandler Accepted;
        public event EventHandler Exited;

        public DelegateCommand AcceptCommand { get; set; }
        public DelegateCommand ExitCommand { get; set; }

        public UnsupportedVersionViewModel()
        {
            AcceptCommand = new DelegateCommand(Accept_OnExecute);
            ExitCommand = new DelegateCommand(Exit_OnExecute);
        }

        private void Accept_OnExecute(object sender)
        {
            Accepted?.Invoke(this, EventArgs.Empty);
        }

        private void Exit_OnExecute(object sender)
        {
            Exited?.Invoke(this, EventArgs.Empty);
        }
    }
}
