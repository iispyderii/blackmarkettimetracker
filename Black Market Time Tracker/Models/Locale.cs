﻿using System.Collections.Generic;

namespace Black_Market_Time_Tracker.Models
{
    public class Locale
    {
        public string baseAddress;
        public string locale;
    }

    public static class LocaleDatabase
    {
        public static Dictionary<string,Locale> GetLocales()
        {
            return new Dictionary<string, Locale>
            {
                { "Deutsch", new Locale() { baseAddress = "eu", locale = "de_DE" } },
                { "English", new Locale() { baseAddress = "us", locale = "en_US" } },
                { "Español (España)", new Locale() { baseAddress = "eu", locale = "es_ES" } },
                { "Español (América Latina)", new Locale() { baseAddress = "us", locale = "es_MX" } },
                { "Français", new Locale() { baseAddress = "eu", locale = "fr_FR" } },
                { "Italiano", new Locale() { baseAddress = "eu", locale = "it_IT" } },
                { "Português", new Locale() { baseAddress = "us", locale = "pt_BR" } },
                { "Русский", new Locale() { baseAddress = "eu", locale = "ru_RU" } },
                { "한국어", new Locale() { baseAddress = "kr", locale = "ko_KR" } },
                { "简体中文", new Locale() { baseAddress = "cn", locale = "zh_CN" } },
                { "繁體中文", new Locale() { baseAddress = "tw", locale = "zh_TW" } }
            };
        }
    }
}
