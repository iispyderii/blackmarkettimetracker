﻿using Black_Market_Time_Tracker.Models;
using Newtonsoft.Json;
using PropertyChanged;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Black_Market_Time_Tracker
{
    [ImplementPropertyChanged]
    public class BlackMarketItem
    {
        public BlackMarketItem(int index, IBlackMarketMarshalItem item, Locale locale)
        {
            createdOn = DateTime.Now;
            this.locale = locale;

            //updateTimer = new DispatcherTimer
            //{
            //    Interval = TimeSpan.FromSeconds(1)
            //};
            //updateTimer.Tick += UpdateTimer_Tick;

            Index = index;
            PopulateProperties(item);
            UpdateTimerAsync(UpdateTimer_Tick, TimeSpan.FromSeconds(1));

            //updateTimer.Start();
        }

        private DateTime createdOn;
        private Locale locale;
        private Timer updateTimer;

        public int Index { get; private set; }

        public ImageSource Icon { get; private set; }

        public string ItemName { get; private set; }

        public uint ItemID { get; private set; }

        public TimeSpan StaticTimeLeft { get; private set; }

        [AlsoNotifyFor(nameof(TimeLeftString), nameof(LocalEndTime))]
        public TimeSpan TimeLeft { get; private set; }

        public string TimeLeftString => TimeLeft.TotalSeconds < 0 ? "Finished!" : $"{TimeLeft:hh\\:mm\\:ss}";

        public DateTime LocalEndTime => DateTime.Now + TimeLeft;

        public GoldAmount CurrentBid { get; private set; }

        public GoldAmount MinimumIncrement { get; private set; }

        public GoldAmount MinimumBid { get; private set; }

        public bool YouHaveHighBid { get; private set; }

        public uint NumberOfBids { get; private set; }

        private void PopulateProperties(IBlackMarketMarshalItem item)
        {
            WebClient wc = new WebClient { Encoding = Encoding.UTF8 };
            var iconURL = locale.baseAddress != "cn"
                ? $"https://{locale.baseAddress}.api.battle.net/wow/item/"
                : "https://api.battlenet.com.cn/wow/item/";
            // You'll need to provide your own key
            string itemAddr = $"{iconURL}{item.Entry}?locale={locale.locale}&apikey={bnetAPIKey}";
            string resp = wc.DownloadString(itemAddr);
            var itemResponse = JsonConvert.DeserializeObject<BNetItemResponse>(resp);

            byte[] data = wc.DownloadData($"http://us.media.blizzard.com/wow/icons/56/{itemResponse.Icon}.jpg");
            using (var ms = new MemoryStream(data))
            {
                Icon = BitmapFrame.Create(ms, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }

            ItemName = itemResponse.Name;
            ItemID = item.Entry;
            StaticTimeLeft = TimeSpan.FromSeconds(item.TimeLeft);
            CurrentBid = new GoldAmount(item.CurrentBid);
            MinimumIncrement = new GoldAmount(item.MinIncrement);
            MinimumBid = new GoldAmount(item.MinBid);
            YouHaveHighBid = Convert.ToBoolean(item.YouHaveHighBid);
            NumberOfBids = item.NumBids;
        }

        private static async Task UpdateTimerAsync(Action onTick, TimeSpan interval)
        {
            while (true)
            {
                onTick?.Invoke();
                await Task.Delay(interval);
            }
        }

        private void UpdateTimer_Tick()
        {
            TimeLeft = StaticTimeLeft - (DateTime.Now - createdOn);
        }
    }
}
