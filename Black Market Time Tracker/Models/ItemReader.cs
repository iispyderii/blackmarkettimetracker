﻿using Black_Market_Time_Tracker.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Black_Market_Time_Tracker
{
    public class ItemReader
    {
        public ItemReader(ProcessEntry process)
        {
            this.wowProcess = process.Process;
            is64Bit = Is64BitProcess();

            if (is64Bit)
            {
                itemStruct = new BlackMarketMarshalItem64();
            }
            else
            {
                itemStruct = new BlackMarketMarshalItem32();
            }
            structSize = Marshal.SizeOf(itemStruct);

            var fvi = FileVersionInfo.GetVersionInfo(process.FileName);
            IsSupportedVersion = Offsets.BuildVersionOffsets.ContainsKey(fvi.FilePrivatePart);
            versionOffsets = IsSupportedVersion ? Offsets.BuildVersionOffsets[fvi.FilePrivatePart] : Offsets.Latest;
            itemStart = is64Bit ? versionOffsets.ItemStart_x64 : versionOffsets.ItemStart;
            itemCount = is64Bit ? versionOffsets.ItemCount_x64 : versionOffsets.ItemCount;
        }

        private int itemStart;
        private int itemCount;
        private bool is64Bit;
        private Process wowProcess;
        private IBlackMarketMarshalItem itemStruct;
        private int structSize;
        private BMOffset versionOffsets;
        
        public bool IsSupportedVersion { get; }

        public List<IBlackMarketMarshalItem> ReadItems()
        {
            var itemList = new List<IBlackMarketMarshalItem>();
            uint numItems = Read<uint>(new IntPtr(itemCount), true);
            if (numItems == 0) return itemList;

            for (int i = 0; i < numItems; i++)
            {
                itemList.Add(ReadItem(i));
            }
            return itemList;
        }


        [DllImport("Kernel32.dll")]
        private static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] buffer, Int32 numBytes, out int lpNumberOfBytesRead);

        [DllImport("Kernel32.dll")]
        private static extern bool IsWow64Process(IntPtr hProcess, ref bool is64);

        private bool Is64BitProcess()
        {
            try
            {
                bool is64 = false;
                if (!IsWow64Process(wowProcess.Handle, ref is64))
                    return false;

                bool isSelf64 = false;
                if (!IsWow64Process(Process.GetCurrentProcess().Handle, ref isSelf64))
                    return false;

                if ((IntPtr.Size == 8 && !isSelf64) || (IntPtr.Size == 4 && isSelf64))
                    return !is64;

                return is64;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private IBlackMarketMarshalItem ReadItem(int index)
        {
            if (is64Bit)
            {
                long BaseAddr = Read<long>(new IntPtr(itemStart), true);
                long itemAddress = BaseAddr + index * structSize;
                return Read<BlackMarketMarshalItem64>(new IntPtr(itemAddress), false);
            }
            else
            {
                int BaseAddr = Read<int>(new IntPtr(itemStart), true);
                int itemAddress = BaseAddr + index * structSize;
                return Read<BlackMarketMarshalItem32>(new IntPtr(itemAddress), false);
            }
        }

        private unsafe T Read<T>(IntPtr addr, bool relative)
        {
            IntPtr readAddr = addr;
            if (relative)
                readAddr = new IntPtr((addr - 0x400000).ToInt64() + wowProcess.MainModule.BaseAddress.ToInt64());
            int size = Marshal.SizeOf<T>();
            byte[] buffer = new byte[size];
            if (!ReadProcessMemory(wowProcess.Handle, readAddr, buffer, size, out int lpNumberOfBytesRead))
                throw new AccessViolationException();
            fixed (byte* ptr = buffer)
                return (T)Marshal.PtrToStructure((IntPtr)ptr, typeof(T));
        }
    }
}
