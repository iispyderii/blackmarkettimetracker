﻿using Newtonsoft.Json;

namespace Black_Market_Time_Tracker.Models
{
    public class BNetItemResponse
    {
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("icon")]
        public string Icon;
    }
}
