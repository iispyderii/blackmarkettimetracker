﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Black_Market_Time_Tracker.Models
{
    internal static class Offsets
    {
        public static BMOffset Latest => BuildVersionOffsets.Last().Value;
        public static int LatestVersion => BuildVersionOffsets.Last().Key;

        public static readonly Dictionary<int, BMOffset> BuildVersionOffsets =
            new Dictionary<int, BMOffset>()
            {
                [16309] = new BMOffset(0x117516C, 0x1175170, 0x14B739C, 0x14B73A0),
                [16357] = new BMOffset(0x1175174, 0x1175178, 0x14B73AC, 0x14B73B0),
                [16669] = new BMOffset(0x11FB820, 0x11FB824, 0x1574ED8, 0x1574EE0),
                [17005] = new BMOffset(0x1188b20, 0x1188b24, 0x188b860, 0x188b868),
                [17371] = new BMOffset(0x11F7E38, 0x11F7E3C, 0, 0),
                [17399] = new BMOffset(0x11F7E38, 0x11F7E3C, 0, 0),
                [17538] = new BMOffset(0x11EE4C8, 0x11EE4CC, 0, 0),
                [17898] = new BMOffset(0x120E270, 0x120E274, 0, 0),
                [18414] = new BMOffset(0x1210758, 0x121075C, 0, 0),
                [19116] = new BMOffset(0x1242DE8, 0x1242DEC, 0, 0),
                [19243] = new BMOffset(0x1242DE8, 0x1242DEC, 0, 0),
                [19342] = new BMOffset(0x1242DE8, 0x1242DEC, 0, 0),
                [19678] = new BMOffset(0x12E0298, 0x12E029C, 0, 0),
                [19702] = new BMOffset(0x12E0298, 0x12E029C, 0, 0),
                [19802] = new BMOffset(0x12922B0, 0x12922B4, 0, 0),
                [19831] = new BMOffset(0x12922B0, 0x12922B4, 0, 0),
                [19865] = new BMOffset(0x12922C0, 0x12922C4, 0, 0),
                [20173] = new BMOffset(0x1350808, 0x135080C, 0, 0),
                [20201] = new BMOffset(0x1350808, 0x135080C, 0, 0),
                [20253] = new BMOffset(0x13507E8, 0x13507EC, 0, 0),
                [20338] = new BMOffset(0x13507E8, 0x13507EC, 0, 0),
                [20444] = new BMOffset(0x135D590, 0x135D594, 0, 0),
                [20886] = new BMOffset(0x135CDA0, 0x135CDA4, 0x1B56528, 0x1B56530),
                [21336] = new BMOffset(0x12536D0, 0x12536D4, 0x19CD6E8, 0x19CD6F0),
                [21355] = new BMOffset(0x12536D0, 0x12536D4, 0x19CE6E8, 0x19CE6F0),
                [21463] = new BMOffset(0x12536D0, 0x12536D4, 0x19CE6E8, 0x19CE6F0),
                [21676] = new BMOffset(0x12536D0, 0x12536D4, 0x19CE6E8, 0x19CE6F0),
                [21742] = new BMOffset(0x12536D0, 0x12536D4, 0x19CE6E8, 0x19CE6F0),
                [22277] = new BMOffset(0x1301F60, 0x1301F64, 0x1BE6FC0, 0x1BE6FC8),
                [22293] = new BMOffset(0x1300C30, 0x1300C34, 0x1BE5CE0, 0x1BE5CE8),
                [22522] = new BMOffset(0x1306EA0, 0x1306EA4, 0x1BEDEC0, 0x1BEDEC8),
                [22566] = new BMOffset(0x1306EA0, 0x1306EA4, 0x1BEDEC0, 0x1BEDEC8),
                [22624] = new BMOffset(0x13070A0, 0x13070A4, 0x1BF0120, 0x1BF0128),
                [22908] = new BMOffset(0x1356B60, 0x1356B64, 0x1C850D0, 0x1C850D8),
                [22995] = new BMOffset(0x1357B80, 0x1357B84, 0x1C860D0, 0x1C860D8),
                [22996] = new BMOffset(0x1357B80, 0x1357B84, 0x1C860D0, 0x1C860D8),
                [23222] = new BMOffset(0x1358BA0, 0x1358BA4, 0x1C86100, 0x1C86108),
                [23360] = new BMOffset(0x135C058, 0x135C05C, 0x1C8F5D0, 0x1C8F5D8),
                [23420] = new BMOffset(0x135C0B8, 0x135C0BC, 0x1C90630, 0x1C90638),
                [23857] = new BMOffset(0x13EA250, 0x13EA254, 0x1D76190, 0x1D76198),
                [23877] = new BMOffset(0x13EA280, 0x13EA284, 0x1D771B0, 0x1D771B8),
                [23911] = new BMOffset(0x13EA2A0, 0x13EA2A4, 0x1D771E0, 0x1D771E8),
                [23937] = new BMOffset(0x13EA2A0, 0x13EA2A4, 0x1D771E0, 0x1D771E8),
                [24015] = new BMOffset(0x13EA2A0, 0x13EA2A4, 0x1D771E0, 0x1D771E8),
                [24330] = new BMOffset(0x140AA10, 0x140AA14, 0x1DB5AD0, 0x1DB5AD8),
                [24367] = new BMOffset(0x140AA10, 0x140AA14, 0x1DB5AD0, 0x1DB5AD8),
                [24430] = new BMOffset(0x140AA10, 0x140AA14, 0x1DB5AD0, 0x1DB5AD8),
                [24742] = new BMOffset(0x140BA50, 0x140BA54, 0x1DB6B20, 0x1DB6B28),
            };
    }
}
