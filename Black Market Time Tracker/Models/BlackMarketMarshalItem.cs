﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Black_Market_Time_Tracker
{
    public interface IBlackMarketMarshalItem
    {
        uint MarketID { get; }
        uint Entry { get; }
        uint Quantity { get; }
        ulong MinBid { get; }
        ulong MinIncrement { get; }
        ulong CurrentBid { get; }
        uint TimeLeft { get; }
        byte YouHaveHighBid { get; }
        uint NumBids { get; }

    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe class BlackMarketMarshalItem32 : IBlackMarketMarshalItem
    {
        public uint MarketID { get; }
        public uint unk1;
        public uint Entry { get; }
        public uint Quantity { get; }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 56)]
        public byte[] unk3;
        public ulong MinBid { get; }
        public ulong MinIncrement { get; }
        public ulong CurrentBid { get; }
        public uint TimeLeft { get; }
        public byte YouHaveHighBid { get; }
        public byte unk16;
        public byte unk17;
        public byte unk18;
        public uint NumBids { get; }
        public uint unk19;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe class BlackMarketMarshalItem64 : IBlackMarketMarshalItem
    {
        public uint MarketID { get; }
        public uint unk1;
        public uint Entry { get; }
        public uint Quantity { get; }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 108)]
        public byte[] unk3;
        public ulong MinBid { get; }
        public ulong MinIncrement { get; }
        public ulong CurrentBid { get; }
        public uint TimeLeft { get; }
        public byte YouHaveHighBid { get; }
        public byte unk16;
        public byte unk17;
        public byte unk18;
        public uint NumBids { get; }
        public uint unk19;
    }
}
