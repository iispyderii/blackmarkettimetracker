﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Black_Market_Time_Tracker
{
    public class ProcessEntry
    {
        public ProcessEntry(Process process)
        {
            try
            {
                Process = process;
                Title = $"{process.ProcessName} (PID: {process.Id})";
                FileName = process.MainModule.FileName;
                Icon icon = System.Drawing.Icon.ExtractAssociatedIcon(FileName);
                Icon = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            }
            catch
            {
                Title = "Acess Denied";
                Icon = Imaging.CreateBitmapSourceFromHBitmap(
                    SystemIcons.Error.ToBitmap().GetHbitmap(), 
                    IntPtr.Zero, Int32Rect.Empty, 
                    BitmapSizeOptions.FromEmptyOptions());
                FileName = "Denied";
            }
        }
        public ImageSource Icon { get; }
        public string Title { get; }
        public string FileName { get; }
        public Process Process { get; }
    }
}
