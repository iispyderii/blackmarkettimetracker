﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Black_Market_Time_Tracker
{
    internal class BMOffset
    {
        public int ItemCount { get; }
        public int ItemStart { get; }
        public int ItemCount_x64 { get; }
        public int ItemStart_x64 { get; }

        public BMOffset(int itemCount, int itemStart, int itemCount_x64 = 0, int itemStart_x64 = 0)
        {
            ItemCount = itemCount;
            ItemStart = itemStart;
            ItemCount_x64 = itemCount_x64;
            ItemStart_x64 = itemStart_x64;
        }
    }
}
