﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Black_Market_Time_Tracker
{
    public class GoldAmount
    {
        public GoldAmount(ulong copper)
        {
            Copper = copper % 100;
            Silver = (copper / 100) % 100;
            Gold = (copper / 10000);
        }

        public ulong Gold { get; set; }
        public ulong Silver { get; set; }
        public ulong Copper { get; set; }
    }
}
